import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class SearchComparison {
	
	// Max. testitud jada pikku
	public static final int MAX = 512000;
	
	public static void main(String[] args) {
		
		int[] testedSizes2 = {32000, 64000, 128000, 192000, 256000, 384000, 512000};
		int[] testedSizes = {64000, 128000, 256000, 512000};
		int parallels = 10;
		for (int i: testedSizes) {
			System.out.println("\nTesting arrays length " + i + "\n") ;			
			compareSearches(i, parallels);			
		}		
	}
	
	private  static void compareSearches(int numerOfObjects, int parallels) {
		List<Integer> m = generateList(numerOfObjects);				
		compareInsertionSort(m, parallels); // Insertion sort
		compareBinaryInsertionSort(m, parallels); // Binary insertion sort
		compareQuickSort(m, parallels); // Quick sort
		compareJavaAPISort1(m, parallels); // JavaAPI Arrays
		compareJavaAPISort2(m, parallels); // JavaAPI Collections
	}
	
	private static int mean(int[] list) {
		int sum = 0;		
		for (int i=0; i<list.length; i++) {
			sum = sum + list[i];        
		}
		return sum / list.length;
	}
	
	private static int std(int[] list) {
		double mean = (double) mean(list);
		double std = 0;	
		for (int i=0; i<list.length; i++) {
			std = std + Math.pow((list[i] - mean),2);        
		}
		std = Math.pow(std / (list.length - 1), 0.5);
		return (int)std;
	}
	
	private  static void compareInsertionSort(List<Integer> m, int parallels) {
		long stime, ftime;
		List<Integer> mcopy = copyList(m);
		int[] results = new int[parallels];
		for (int i=0; i<parallels; i++) {
			stime = new Date().getTime();
			SortingTask.insertionSort(mcopy, 0, mcopy.size());
			ftime = new Date().getTime();
			results[i] = new Long (ftime-stime).intValue();        
		}
        System.out.println ("Insertion sort (ms): " + String.valueOf(mean(results)) + " ( +- " + String.valueOf(std(results)) + " )");
	}
	
	private  static void compareBinaryInsertionSort(List<Integer> m, int parallels) {
		long stime, ftime;
		List<Integer> mcopy = copyList(m);
		int[] results = new int[parallels];
		for (int i=0; i<parallels; i++) {
			stime = new Date().getTime();
			SortingTask.binaryInsertionSort(mcopy, 0, mcopy.size());
			ftime = new Date().getTime();
			results[i] = new Long (ftime-stime).intValue();        
		}
        System.out.println ("Binary insertion sort (ms): " + String.valueOf(mean(results)) + " ( +- " + String.valueOf(std(results)) + " )");
	}
	
	private  static void compareQuickSort(List<Integer> m, int parallels) {
		long stime, ftime;
		List<Integer> mcopy = copyList(m);
		int[] results = new int[parallels];
		for (int i=0; i<parallels; i++) {
			stime = new Date().getTime();
			SortingTask.qsort(mcopy, 0, mcopy.size());
			ftime = new Date().getTime();
			results[i] = new Long (ftime-stime).intValue();        
		}
		System.out.println ("Quick sort (ms): " + String.valueOf(mean(results)) + " ( +- " + String.valueOf(std(results)) + " )");
	}
	
	private  static void compareJavaAPISort1(List<Integer> m, int parallels) {
		long stime, ftime;
		List<Integer> mcopy = copyList(m);
		int[] results = new int[parallels];
		for (int i=0; i<parallels; i++) {
			Integer[] sarray = new Integer[mcopy.size()];
	        sarray = (Integer[])mcopy.toArray (sarray);
	        stime = new Date().getTime();					
			Arrays.sort(sarray, 0, mcopy.size());
			ftime = new Date().getTime();
			results[i] = new Long (ftime-stime).intValue();        
		}
        System.out.println ("Java API sort using Arrays (ms): " + String.valueOf(mean(results)) + " ( +- " + String.valueOf(std(results)) + " )");
	}	
	
	private  static void compareJavaAPISort2(List<Integer> m, int parallels) {
		long stime, ftime;
		List<Integer> mcopy = copyList(m);
		int[] results = new int[parallels];
		for (int i=0; i<parallels; i++) {
			stime = new Date().getTime();			
			Collections.sort(mcopy);
			ftime = new Date().getTime();
			results[i] = new Long (ftime-stime).intValue();        
		}
        System.out.println ("Java API sort using Collection (ms): " + String.valueOf(mean(results)) + " ( +- " + String.valueOf(std(results)) + " )");
	}	
	
	private static List<Integer> generateList(int n) {
		List<Integer> intList = new ArrayList<Integer>(n);
		Random generator = new Random();
		
		// Max j�rjestatatava arvu v��rtus
		int maxKey = Math.min (1000, (MAX+32)/16);
		
		// Genereeri jada
		for (int i = 0; i < n; i++) {
			intList.add(new Integer(generator.nextInt(maxKey)));
		}
		
		return intList;
	}
	
	private static List<Integer> copyList(List<Integer> m) {
		List<Integer> mcopy = new ArrayList<Integer>(m.size());
		for (int i = 0; i < m.size(); i++) {
			mcopy.add(new Integer(m.get(i).intValue()));
		}		
		return mcopy;
	}
	
}
